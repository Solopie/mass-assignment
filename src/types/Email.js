class Email {
    constructor(email) {
        this.validate(email);
        this.email = email;
    }

    validate(email) {
        if (typeof email !== "string") {
            throw "Email needs to be a string";
        }
        
        if (email.length > 200) {
            throw "Email must be less than 200 characters";
        }

        var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if(!pattern.test(email)) {
            throw "Email is not valid format";    
        }
    }
}

module.exports = { Email }
