class Name {
    constructor(name) {
        this.validate(name);
        this.name = name;
    }

    validate(name) {
        if(typeof name !== "string") {
            throw "Name needs to be a string";
        }

        const pattern = /^[a-z ,.'-]+$/i;
        if(!pattern.test(name)) {
            throw("Regex not satisfied for name")
        }

        if(name.length > 200) {
            throw "Name too long";
        }
    }
}

module.exports = { Name };
