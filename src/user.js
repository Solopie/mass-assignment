// User data model

var User = {
    id: auth_user_id(),
    name: "",
    email: "",
    role: "user"
}

// A psudo authenticator 
// returns authenticated user ID
function auth_user_id() {
    return 2;
}


module.exports = { User, auth_user_id };
